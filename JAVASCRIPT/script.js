var setBtn = true;
var stopBtn = false;
var clearBtn = false;
var start;

let myVar = setInterval(function () { myTimer() }, 1000);
function myTimer() {
        var d = new Date();
        var t = d.toDateString() + " " + d.toLocaleTimeString();
        document.getElementById("demo").innerHTML = t;
}

function startDate() {

        if (setBtn) {
                start = new Date();
                var startDate = start.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
                document.getElementById("start").innerHTML = startDate;
                document.getElementById("startBtn").innerHTML = '<i class="mdi mdi-stop text-lg"> </i> Stop';
                document.getElementById("startBtn").style.backgroundColor = "red"; 
                setBtn = false;
                stopBtn = true;
        } else if (stopBtn) {
                var stop = new Date();
                var stopDate = stop.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
                document.getElementById("stop").innerHTML = stopDate;
                var total = (stop.getHours()*60+stop.getMinutes()) - (start.getHours()*60+start.getMinutes());
                document.getElementById("total").innerHTML = total;
                
                if (total <= 15) {
                        document.getElementById("payment").innerHTML = "500"
                } else if (total <= 30) {
                        document.getElementById("payment").innerHTML = "1000"
                }
                else if (total <= 60) {
                        document.getElementById("payment").innerHTML = "1500"
                }
                else {
                        var minutes = total % 60;
                        var min = Math.trunc(total / 60);

                        if (minutes > 0 && minutes <= 15) {
                                document.getElementById("payment").innerHTML = (min * 1500) + 500;
                                console.log(min)
                        }
                        else if(minutes > 0 && minutes <=30){
                                document.getElementById("payment").innerHTML = (min * 1500) + 1000;
                                console.log(min)
                        }
                        else if(minutes > 0 && minutes <60){
                                document.getElementById("payment").innerHTML = (min * 1500) + 1500;
                                console.log(min)
                        }
                        else {
                                document.getElementById("payment").innerHTML = (min * 1500);
                                console.log(min)  
                        }
                }
                document.getElementById("startBtn").innerHTML = '<i class="mdi mdi-trash-can  text-lg p-3"></i>Clear';
                document.getElementById("startBtn").style.backgroundColor = "orange"; 
                stopBtn = false;
                clearBtn = true;
        } else {
                document.getElementById("start").innerHTML = '00:00';
                document.getElementById("stop").innerHTML = '00:00';
                document.getElementById("total").innerHTML = '0';
                document.getElementById("payment").innerHTML = '0';

                document.getElementById("startBtn").innerHTML = '<i class="mdi mdi-play  text-lg p-3"></i>Start';
                document.getElementById("startBtn").style.backgroundColor = "green"; 
                clearBtn = false;
                setBtn = true;
        }
}

