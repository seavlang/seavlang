tailwind.config = {
    theme: {
      extend: {
        fontFamily: {
          font: ["Aclonica", "sans-serif"],
          font1: ["Fraunces","serif"]
        },
        backgroundImage: {
          bg1: "url(https://wallpapercave.com/wp/wp7505170.jpg)",
        },
        colors: {
          head: "#874356",
          start: "#C65D7B",
          pay: "#F68989",
          button: "#FFDDD2"
        },
        },
    },
  };